package pvt.titas.spring.loadsetofbean;

import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava.Range;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnJava(value = JavaVersion.EIGHT, range = Range.EQUAL_OR_NEWER)
public class ForNewerJavaVersionConfig {

	@Bean
	public JavaComponent javaComponent() {
		return new JavaEightComponent();
	}
}
