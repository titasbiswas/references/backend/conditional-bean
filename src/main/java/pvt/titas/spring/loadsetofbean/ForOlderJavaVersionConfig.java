package pvt.titas.spring.loadsetofbean;

import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava.Range;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnJava(value = JavaVersion.EIGHT, range = Range.OLDER_THAN)
public class ForOlderJavaVersionConfig {

	@Bean
	public JavaComponent javaComponent() {
		return new JavaSevenComponent();
	}

}
