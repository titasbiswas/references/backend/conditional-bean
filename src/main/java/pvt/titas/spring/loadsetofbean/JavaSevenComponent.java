package pvt.titas.spring.loadsetofbean;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class JavaSevenComponent implements JavaComponent{
	
	public void printList() {
		List<String> lst = Arrays.asList("John", "Maria", "Neha", "Venkat");
		
		
		for(String string: lst) {
			System.out.println(string);
		}

	}

}
