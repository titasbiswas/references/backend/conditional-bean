package pvt.titas.spring.loadsetofbean;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class JavaEightComponent implements JavaComponent{
	
	public void printList() {
		List<String> lst = Arrays.asList("John", "Maria", "Neha", "Venkat");		
		lst.forEach(System.out::println);

	}

}
