package pvt.titas.spring.singlebeaninit;

import java.util.List;

public interface SimpleDataSource {

	public List<String> getData();
}
