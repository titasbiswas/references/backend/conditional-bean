package pvt.titas.spring.singlebeaninit;

import java.util.List;

public interface SimpleService {

	List<String> getData();
}
