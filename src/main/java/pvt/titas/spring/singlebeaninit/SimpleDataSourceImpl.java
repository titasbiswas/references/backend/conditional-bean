package pvt.titas.spring.singlebeaninit;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class SimpleDataSourceImpl implements SimpleDataSource{

	@Override
	public List<String> getData() {
		return Arrays.asList("John", "Maria", "Neha", "Venkat");
	}
	
	

}
