package pvt.titas.spring.singlebeaninit;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SingleBeanInitConfig {

	@Bean(name = "ds")
	public SimpleDataSource dataSource() {
		return new SimpleDataSourceImpl();
	}
	
	
	
	/**
	 * IMPORTANT - REQUIRED BEAN TO BE LOADED FIRST, I.E TO BE DECLARED FIRST
	 */
	@Bean(name = "simpleService")
	@ConditionalOnBean(name = "ds")
	public SimpleService simpleService() {
		System.out.println(this.getClass().toGenericString()+" has been initialized");
		return new SimpleServiceImpl();
	}
	
	
	/**
	 * THIS WILL NOT LOAD AS IT ds2 IS NOT LOADED YET
	 */
	@Bean(name = "simpleService2")
	@ConditionalOnBean(name = "ds2")
	public SimpleService simpleService2() {
		System.out.println(this.getClass().toGenericString()+" has been initialized");
		return new SimpleServiceImpl();
	}
	
	@Bean(name = "ds2")
	public SimpleDataSource dataSource2() {
		return new SimpleDataSourceImpl();
	}
	
	
	
}
