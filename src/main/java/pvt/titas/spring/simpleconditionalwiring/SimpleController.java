package pvt.titas.spring.simpleconditionalwiring;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simplebean")
public class SimpleController {
	
	@Autowired
	private SimpleService simpleService;
	
	@GetMapping
	public ResponseEntity<Map<String, String>> getBeanName(){
		Map<String, String> responseMap = new HashMap<>();
		responseMap.put("bean-name", simpleService.returnAString());
		return ResponseEntity.ok(responseMap);
	}

}
