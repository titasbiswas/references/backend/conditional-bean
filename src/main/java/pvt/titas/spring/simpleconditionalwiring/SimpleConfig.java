package pvt.titas.spring.simpleconditionalwiring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleConfig {

	@Value("${simple.init.bean}")
	private String beanName;
	
	@Bean
	public SimpleService simpleService() {
		if(beanName.equalsIgnoreCase("one"))
			return new SimpleServiceImplOne();
		else
			return new SimpleServiceImplTwo();
	}
}
