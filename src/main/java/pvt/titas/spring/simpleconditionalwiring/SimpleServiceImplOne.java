package pvt.titas.spring.simpleconditionalwiring;

import org.springframework.stereotype.Service;

@Service
public class SimpleServiceImplOne implements SimpleService {

	@Override
	public String returnAString() {
		return "From Bean One";
	}

}
