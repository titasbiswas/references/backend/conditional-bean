package pvt.titas.spring.simpleconditionalwiring;

import org.springframework.stereotype.Service;

@Service
public class SimpleServiceImplTwo implements SimpleService{

	@Override
	public String returnAString() {
		return "From Bean Two";
	}

}
