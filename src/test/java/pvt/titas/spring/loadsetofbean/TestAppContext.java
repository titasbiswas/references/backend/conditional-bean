package pvt.titas.spring.loadsetofbean;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ForNewerJavaVersionConfig.class,ForOlderJavaVersionConfig.class})
public class TestAppContext {

	@Autowired
	JavaComponent javaComponent;
	
	@Test
	public void testAutoLoading() {
		System.out.println(javaComponent.getClass().getName());
		Assert.assertNotNull(javaComponent);
	}
}
