package pvt.titas.spring.singlebeaninit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SingleBeanInitConfig.class)
public class TestAppContext {
	
	@Autowired
	@Qualifier("simpleService")
	SimpleService simpleService;
	
	@Autowired(required = false)
	@Qualifier("simpleService2")
	SimpleService simpleService2;
	
	@Test
	public void testContextLoad() {
		Assert.assertNotNull(simpleService);
	}
	
	@Test
	public void testContextLoadError() {
		Assert.assertNull(simpleService2);
	}

}
